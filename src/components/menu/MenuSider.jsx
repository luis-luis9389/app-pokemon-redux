import React, { useState } from 'react';
import { Link } from 'react-router-dom'

const MenuSider = () => {

  const [showMenu, setShowMenu] = useState(false);

  return (
    <div className="menu-sider">
      <i className="fas fa-bars icono-movil" onClick={() => setShowMenu(!showMenu)} ></i>
      <nav className={` ${showMenu ? 'menu-sider__nav-active' : null} menu-sider__nav`}>
        <ul className="menu-sider__ul">
          <li className="menu-sider__li">
            <Link to="/"> Home </Link>
          </li>
          <li className="menu-sider__li">
            <Link to='/search'> Search </Link>
          </li>
        </ul>
      </nav>
    </div>
  )
}

export default MenuSider
