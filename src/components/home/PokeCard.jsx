import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { startLoading } from '../../actions/ui';

const PokeCard = ({ poke }) => {

  const dispatch = useDispatch();

  return (
    <Link onClick={()=> dispatch(startLoading()) } style={{ textDecoration: 'none', color: 'black' }} to={`/pokemon/${poke.id}`}>
      <div className="pokelist__item ">
        <div className="pokemon__description">
          <span># {poke.id}</span>
          <span>{poke.name}</span>
        </div>
        <LazyLoadImage
          className='animate__animated animate__zoomIn'
          alt={poke.name}
          src={poke.sprites.other["official-artwork"].front_default}
        />
      </div>
    </Link>
  )
}

export default PokeCard
