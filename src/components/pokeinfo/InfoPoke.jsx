import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import Loader from '../home/Loader';

const InfoPoke = () => {

  const [typecolor, setTypeColor] = useState({})

  const { loading } = useSelector(state => state.ui);
  const { pokemonById } = useSelector(state => state.pokemons);
  const { pokemonEvolutions } = useSelector(state => state.pokemons);
  const { pokemonDetails } = useSelector(state => state.pokemons);


  const pokeById = pokemonById[0];
  const evolutions = pokemonEvolutions[0];

  useEffect(() => {
    setTypeColor(pokeById.types.map(t => (t.type.name)))
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <section>
      <div className={` infopoke__img ${typecolor[0]} `} >
        <LazyLoadImage
          className='animate__animated animate__zoomIn'
          alt={pokeById.name}
          src={pokeById.sprites.other["official-artwork"].front_default}
        />
        <div className="infopoke__list">
          <span># {pokeById.id}</span>
          <span>{pokeById.name}</span>
          {
            pokeById.types.map((p, i) => (
              <span className={`${typecolor[i]} tipo `} key={i} >{p.type.name}</span>
            ))
          }
        </div>
      </div>
      <section className="infopoke__details">
        <h2>{pokemonDetails[0].genera[5].genus}</h2>
        <p>{pokemonDetails[0].flavor_text_entries[1].flavor_text}</p>
        <h3>Hábitat - {pokemonDetails[0].habitat.name} </h3>
        <h3>Shape - {pokemonDetails[0].shape.name} </h3>
        <h3>Growth Rate - {pokemonDetails[0].growth_rate.name} </h3>
        <h3>Egg Groups</h3>
        {
          pokemonDetails[0].egg_groups.map((egg, i) => (
            <span key={i}> {egg.name} </span>
          ))
        }
      </section>
      <Evolutions evolutions={evolutions} loading={loading} typecolor={typecolor} />
    </section>
  )
}

const Evolutions = ({ evolutions, loading, typecolor }) => {

  return (
    <section className="infopoke__evolutions">
      <h2>Evoluciones</h2>
      {
        loading === true ?
          (<Loader />)
          :
          (
            <div className="infopoke__evolutions--details">
              {
                evolutions.map((evol, i) => (
                  <div key={i} className="infopoke__evolutions--img">
                    <LazyLoadImage
                      className={`animate__animated animate__zoomIn ${typecolor[0]}`}
                      alt={evol.name}
                      src={evol.sprites.other["official-artwork"].front_default}
                    />
                    <div className="infopoke__types">
                    <span># {evol.id}</span>
                      <span>{evol.name}</span>
                    </div>
                  </div>
                ))
              }
            </div>
          )
      }
    </section>
  )
}

export default InfoPoke
