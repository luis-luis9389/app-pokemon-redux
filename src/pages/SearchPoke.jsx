import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getPokemonSearch } from '../actions/pokemons';
import { setError, removeError } from '../actions/ui';
import Header from '../components/home/Header'
import PokeSearch from '../components/search/PokeSearch'

const SearchPoke = () => {

  const dispatch = useDispatch();

  const { pokemonSearch } = useSelector(state => state.pokemons)

  const [formValue, setFormValue] = useState({
    nameOrId: ''
  });

  const { nameOrId } = formValue;

  const onSubmit = (e) => {
    e.preventDefault();
    if (nameOrId.trim() === '') {
      dispatch(setError('El campo no debe de estar vacio.'));
      setTimeout(() => {
        dispatch(removeError());
      }, 3000);
      return
    }

    dispatch(getPokemonSearch(nameOrId))

    setFormValue({
      nameOrId: ''
    })
  }

  return (
    <>
      <Header />
      <PokeSearch
        onSubmit={onSubmit}
        formValue={formValue}
        setFormValue={setFormValue}
        pokemonSearch={pokemonSearch}
      />
    </>
  )
}

export default SearchPoke
