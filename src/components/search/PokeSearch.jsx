import React from 'react';
import { useSelector } from 'react-redux';
import CardSearch from './CardSearch';

const PokeSearch = ({ formValue, setFormValue, onSubmit, pokemonSearch }) => {

  const { msgError } = useSelector(state => state.ui)

  const { nameOrId } = formValue;

  const handleChange = (e) => {
    setFormValue({
      ...formValue,
      [e.target.name]: e.target.value
    })
  };

  return (
    <>
      <form onSubmit={onSubmit} className="form__search">
        {
          msgError !== 0 ? <p className={`${msgError != null ? 'form__search--warning' : null}`}>{msgError}</p> : null
        }
        <div className="form__search--input-container">
          <input
            type="text"
            placeholder="Nombre o Id"
            autoComplete="off"
            name='nameOrId'
            onChange={handleChange}
            value={nameOrId}
          />
        </div>
        <div className="form__search--input-container">
          <button
            type="submit"
          >
            Buscar
          </button>
        </div>
      </form>
      {
        pokemonSearch.length === 0 ?
          <p style={{ textAlign: 'center', fontSize:'1.5rem' }} >Busca algo...</p>
          :
          <CardSearch pokemonSearch={pokemonSearch} />
      }
    </>
  )
}

export default PokeSearch
