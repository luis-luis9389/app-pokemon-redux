export const types = {
  // pokemons
  getPokemons: 'Get Pokemons',
  getPokemonById: 'Get Pokemons By Id',
  getPokemonEvolutions: 'Get Pokemon Evolutions',
  getPokemonDetails: 'Get Pokemon Details',
  getPokemonSearch: 'Get Pokemon Search',
  nextPage: 'Next Page',
  backPage: 'Back Page',

  //ui
  uiStartLoading: 'Start loading',
  uiFinishLoading: 'Finish loading',
  uiSetError: 'Set Error',
  uiRemoveError: 'Remove Error',
};