import React from 'react';
import { useSelector } from 'react-redux';
import Loader from './Loader';
import PokeCard from './PokeCard';

const PokeList = () => {

  const pokemons = useSelector(state => state.pokemons.pokemons)
  const loading = useSelector(state => state.ui.loading)

  if (loading === true) {
    return (
      <Loader />
    )
  }

  const pokemones = pokemons[0].pokemones;

  return (
    <section className="pokelist container">
      <div className="pokelist-grid">
        {
          pokemones.map(poke => (
            <PokeCard key={poke.id} poke={poke} />
          ))
        }
      </div>
    </section>
  )
}

export default PokeList
