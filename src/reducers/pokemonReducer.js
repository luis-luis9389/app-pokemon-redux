import { types } from '../types/types';

const initialState = {
  pokemons: [],
  pokemonById: [],
  pokemonEvolutions: [],
  pokemonDetails:[],
  pokemonSearch: [],
  page: 0
}

export const pokemonReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.getPokemons:
      return {
        ...state,
        pokemons: [action.payload],
        pokemonSearch: []
      }

    case types.getPokemonById:
      return {
        ...state,
        pokemonById: [action.payload]
      }

    case types.getPokemonEvolutions:
      return {
        ...state,
        pokemonEvolutions:[ action.payload ]
      }

    case types.getPokemonDetails:
      return {
        ...state,
        pokemonDetails: [ action.payload ]
      }

    case types.getPokemonSearch:
      return {
        ...state,
        pokemonSearch: [ action.payload ]
      }

    case types.nextPage:
      return {
        ...state,
        page: action.payload
      }

    case types.backPage:
      return {
        ...state,
        page: action.payload
      }

    default:
      return state;
  }
}