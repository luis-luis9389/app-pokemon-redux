import React from 'react';
import { Link } from 'react-router-dom';
import Logo from '../../images/pokelogo.svg';

const Header = () => {

  return (
    <section>
      <header className="header container">
        <Link to="/">
          <img className="header__img" src={Logo} alt="" />
        </Link>
      </header>
    </section>
  )
}

export default Header
