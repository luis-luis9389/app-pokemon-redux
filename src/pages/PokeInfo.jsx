import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { getPokemonById } from '../actions/pokemons';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../components/home/Loader';
import InfoPoke from '../components/pokeinfo/InfoPoke';

const PokeInfo = () => {

  const { loading } = useSelector(state => state.ui);

  const dispatch = useDispatch();

  const { id } = useParams();

  useEffect(() => {
    dispatch(getPokemonById(id));
  }, [dispatch, id]);

  // useEffect(() => {
  //   // setTimeout(() => {
  //   //   dispatch(getPokemons())
  //   // }, 5000);
  //   dispatch(getPokemons())
  // }, [dispatch])

  if (loading === true) {
    return (
      <Loader />
    )
  }

  return (
    <InfoPoke />
  )
}

export default PokeInfo
