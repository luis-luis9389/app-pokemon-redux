import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import MenuSider from '../components/menu/MenuSider';
import Home from '../pages/Home';
import PokeInfo from '../pages/PokeInfo';
import SearchPoke from '../pages/SearchPoke';

const AppRouter = () => {
  return (
    <Router>
      <div>
        <MenuSider/>
        <Switch>
          <Route exact path='/' component={ Home } />
          <Route exact path='/pokemon/:id' component={ PokeInfo } />
          <Route exact path='/search' component={ SearchPoke } />
          <Redirect to='/' />
        </Switch>
      </div>
    </Router>
  )
}

export default AppRouter
