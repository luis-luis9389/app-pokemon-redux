import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { useSelector } from 'react-redux';
import Loader from '../home/Loader';

const CardSearch = ({ pokemonSearch }) => {
  const { loading } = useSelector(state => state.ui)
  const { msgError } = useSelector(state => state.ui)
  console.log(msgError);
  const pokemon = pokemonSearch[0];

  return (
    <>
      {
        loading === true ?
          (<Loader />)
          :
          (<Card pokemon={pokemon} />)
      }
    </>
  )
}

const Card = ({ pokemon }) => {
  return (
    <section className="section-card__pokemon">
      <div className="card-pokemon">
        <LazyLoadImage
          className='card-pokemon__img animate__animated animate__zoomIn'
          alt={pokemon.name}
          src={pokemon.sprites.other["official-artwork"].front_default}
        />
        <span className='card-pokemon__name'>#{pokemon.id}</span>
        <span className='card-pokemon__name'>{pokemon.name}</span>
      </div>
    </section>
  )
}

export default CardSearch