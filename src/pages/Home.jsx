import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux';
import Header from '../components/home/Header';
import PokeList from '../components/home/PokeList';
import { getPokemons, nextPage, backPage } from '../actions/pokemons';
import { useSelector } from 'react-redux';

const Home = () => {

  const { page } = useSelector(state => state.pokemons);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getPokemons(page));
  }, [dispatch,page]);

  const pageNext = () => {
    dispatch(nextPage(page + 20))
  }

  const pageBack = () => {
    dispatch(backPage(page - 20))
  }

  return (
    <section>
      <Header />
      <PokeList />
      <div className="button__navigation">
        <button
          disabled={page === 0 ? `{true}` : null}
          onClick={pageBack}
          className={ page === 0 ? 'btn-disabled' : null }
        >
          Back
        </button>
        <button
          onClick={pageNext}
        >
          Next
        </button>
      </div>
    </section>
  )
}

export default Home
