import { types } from '../types/types';
import { startLoading, finishLoading, setError, removeError } from './ui';

export const getPokemons = (page) => {
  let pokemones = [];
  return async (dispatch) => {
    dispatch(startLoading())
    const res = await fetch(`https://pokeapi.co/api/v2/pokemon/?offset=${page}&limit=20`);
    const pokemons = await res.json();

    for (let i = 0; i < pokemons.results.length; i++) {
      const res = await fetch(pokemons.results[i].url),
        pokes = await res.json();
      pokemones.push(pokes)
    }
    dispatch(fetchPokemons(pokemones));
    dispatch(finishLoading());
  }
}


export const fetchPokemons = (pokemones) => {
  return {
    type: types.getPokemons,
    payload: {
      pokemones
    }
  }
}

export const nextPage = (numero) => {
  return {
    type: types.nextPage,
    payload: numero
  }
}

export const backPage = (numero) => {
  return {
    type: types.backPage,
    payload: numero
  }
}

export const getPokemonById = (id) => {
  return async (dispatch) => {
    dispatch(startLoading())
    const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`)
    const pokemonById = await res.json();
    dispatch(pokemonId(pokemonById));

    //evolutions
    const controller = new AbortController();
    const signal = controller.signal;

    let evolutionOne = [];
    let evolutionTwo = [];
    let evolutionThree = [];
    let evolucion2;
    let evolucion3;
    // let details;
    let allDetails;

    const data1 = await fetch(pokemonById.species.url),
      pokemon_species = await data1.json();
    // details = pokemon_species.flavor_text_entries[0].flavor_text;
    allDetails = pokemon_species;

    const data2 = await fetch(pokemon_species.evolution_chain.url),
      evolution_chain = await data2.json();

    const data3 = await fetch(evolution_chain.chain.species.url),
      evolution_species = await data3.json()

    const pokemon_species1 = await fetch(evolution_species.varieties[0].pokemon.url),
      evolution1 = await pokemon_species1.json();
    evolutionOne.push(evolution1);

    if (evolution_chain.chain.evolves_to.length === 0) {
      controller.abort();
    } else {
      const data4 = await fetch(evolution_chain.chain.evolves_to[0].species.url, { signal }),
        evolution_species2 = await data4.json();

      const data5 = await fetch(evolution_species2.varieties[0].pokemon.url),
        evolution2 = await data5.json();
      evolucion2 = evolution2;
      evolutionTwo.push(evolucion2);
    }

    if (evolution_chain.chain.evolves_to.length === 0 || evolution_chain.chain.evolves_to[0].evolves_to.length === 0) {
      controller.abort();
    } else {
      const data6 = await fetch(evolution_chain.chain.evolves_to[0].evolves_to[0].species.url, { signal }),
        evolution_species3 = await data6.json();

      const data7 = await fetch(evolution_species3.varieties[0].pokemon.url),
        evolution3 = await data7.json();
      evolucion3 = evolution3;
      evolutionThree.push(evolucion3);
    }
    const evolutions = evolutionOne.concat(evolutionTwo, evolutionThree);
    dispatch(pokemonDetails(allDetails))
    dispatch(pokemonEvolutions(evolutions))
    dispatch(finishLoading());
  }
}

const pokemonId = (pokemonById) => {
  return {
    type: types.getPokemonById,
    payload: pokemonById
  }
}

const pokemonEvolutions = (evolutions) => {
  return {
    type: types.getPokemonEvolutions,
    payload: evolutions
  }
}

const pokemonDetails = (details) => {
  return {
    type: types.getPokemonDetails,
    payload: details
  }
}

export const getPokemonSearch = (nameOrId) => {
  let result;
  return async (dispatch) => {
    dispatch(startLoading());
    dispatch(removeError())
    try {
      const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${nameOrId}/`);
      const searchResult = await res.json();
      result = searchResult;
      dispatch(searchPoke(result));
      dispatch(finishLoading());
    } catch (error) {
      dispatch(startLoading());
      dispatch(setError(`El pokemon ${nameOrId} no existe, intenta de nuevo`));
      // setTimeout(() => {
      //   dispatch(removeError())
      // }, 3000);
      dispatch(finishLoading());
    }
  }
}

const searchPoke = (result) => {
  return {
    type: types.getPokemonSearch,
    payload: result
  }
}
