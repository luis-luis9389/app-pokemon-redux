import React from 'react';
import Pokeball from '../../images/pokeball.png';

const Loader = () => {
  return (
    <div className="spinner">
      <img src={Pokeball} alt="pokeball spinner"/>
    </div>
  )
}

export default Loader
